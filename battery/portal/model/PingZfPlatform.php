<?php
/**
 * Created by PhpStorm.
 * User: liukun
 * Date: 2018/9/28
 * Time: 下午12:32
 */
namespace battery\portal\model;


use think\Config;

class PingZfPlatform
{
    function __construct()
    {
        vendor('pingzf.init');
        \Pingpp\Pingpp::setApiKey(Config::get('pingzf_apikey'));
        \Pingpp\Pingpp::setPrivateKeyPath('../data/conf/pingzf_rsa_private_key.pem');
        //设置自定义 CA 证书路径
        \Pingpp\Pingpp::$caBundle = '/path/to/cacert.pem';
    }
    function get_client_ipx()
    {
        $cip = "unknown";
        if ($_SERVER['REMOTE_ADDR'])
        {
            $cip = $_SERVER['REMOTE_ADDR'];
        }
        elseif (getenv("REMOTE_ADDR"))
        {
            $cip = getenv("REMOTE_ADDR");
        }
        return $cip;
}
    //支付
    function createCharge($pay='',$goodname='',$channel='',$ip,$openid=''){

        $pingZFdata=array(
            'order_no'  => $pay['order_num'],
            'amount'    => $pay['price'],
            'app'       => array('id' => Config::get('pingzf_appid')),
            'channel'   => $channel,
            'extra'     => $channel,
            'time_expire'   =>$pay['create_time']+1800 ,//默认30分钟有效期
            'currency'  => 'cny',
            'client_ip' => $ip,
            'subject'   => $goodname,
            'body'      => $goodname
        );
        switch ($channel){
            case 'wx_pub':
                $pingZFdata['extra']=array(
                    'open_id'=>$openid?$openid:''
                );
                break;
            case 'alipay_wap':
                $pingZFdata['extra']=array(
                    'success_url'=>$pay['success_url']?$pay['success_url']:''
                );
                break;

        }
        $info=\Pingpp\Charge::create($pingZFdata);
       
        return $info;
    }
    
    


    //退款
    function createRefund($pay=''){

        $jsonRefund=json_decode($pay->success_data,true);;
        $ch = \Pingpp\Charge::retrieve($jsonRefund['data']['object']['id']);
        $re = $ch->refunds->create(
            array('description'=>'退款')
        );
        return $re;
    }

    //取消待支付退款
    function createReverse($pay=''){
        $jsonRefund=json_decode($pay->pay_data,true);;
        $charge = \Pingpp\Charge::reverse($jsonRefund['id']);
        return $charge;
    }



}