<?php

namespace battery\portal\model;

use think\cache\driver\Redis;

class RedisModel extends Redis
{
    protected $options = [
        'host'       => 'localhost',
        'port'       => 6379,
        'password'   => '',
        'select'     => 0,
        'timeout'    => 0,
        'expire'     => 0,
        'persistent' => false,
        'prefix'     => 'battery_',
    ];
    

    
    
}
