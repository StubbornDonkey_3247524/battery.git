<?php
/**
 * Created by PhpStorm.
 * User: liukun
 * Date: 2018/9/28
 * Time: 下午12:32
 */

namespace battery\portal\model;

use think\Config;
class SdPlatform
{

    var $url='http://mo2oceshi.sudianwang.com/index.php/WebInter/';
    var $zh='test';
    var $pwd='test17810';
    var $safecode='testqwijdnshlena';
    var $extime='172800';


    //电瓶匹配
    function weblcMatch($car_id=0){
        $rinfo=array('status'=>0,'info'=>'');
        $arrinfo=array(
            'sign'=>md5($this->zh.$this->pwd.$this->safecode),
            'car_id'=>$car_id,
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WeblcMatch',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
            $rinfo['info']= $result['data'];
            if(count($rinfo['info'])>0){
                $batteryBaseModel=new BatteryBaseModel();
                $time=time();
                foreach ($rinfo['info'] as $v){
                    $price=bcmul($v['price'],100,0);
                    $md5=md5($v['battery_brand'].$v['battery_model'].$price.$v['id'].'1');
                    $whrer=array();
                    $whrer['md5']=$md5;
                    $batteryBaseModel->startTrans();
                    $order=$batteryBaseModel->name('good')->where($whrer)->find();
                    $batteryBaseModel->commit();
                    if(!$order){
                        $insert=array(
                            'brand'=>$v['battery_brand'],
                            'model'=>$v['battery_model'],
                            'price'=>$price,
                            'create_time'=>$time,
                            'gid'=>$v['id'],
                            'md5'=>$md5,
                        );
                        $batteryBaseModel->name('good')->insert($insert);
                    }

                }

            }

        }
        return $rinfo;
    }


    //获取品牌列表
    function searchBrand()
    {
        $rinfo=array('status'=>0,'info'=>'');

//        $redisModel=new RedisModel();
//        $redinfo=$redisModel->get('searchBrand');
//        if($redinfo){
//            $rinfo['status']=1;
//            $rinfo['info']= $redinfo;
//            return;
//        }

        $arrinfo=array(
                'sign'=>md5($this->zh.$this->pwd.$this->safecode),
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WebSearchBrand',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
            $sdata=$this->getGroupData($result['data']);
//            $redisModel->set('searchBrand',$sdata,$this->extime);
            $rinfo['info']= $sdata;
        }
        return $rinfo;

    }

    //获取车型列表
    function searchSeries($name='')
    {
        $rinfo=array('status'=>0,'info'=>'');
//        $redisModel=new RedisModel();
//        $tag=md5('searchSeries'.$name);
//        $redinfo=$redisModel->get($tag);
//        if($redinfo){
//            $rinfo['status']=1;
//            $rinfo['info']= $redinfo;
//            return;
//        }


        $arrinfo=array(
            'sign'=>md5($this->zh.$this->pwd.$this->safecode),
            'brand'=>$name,
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WebSearchSeries',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
//            $redisModel->set($tag,$result['data'],$this->extime);
            $rinfo['info']= $result['data'];
        }
        return $rinfo;

    }

    //获取车型列表
    function searchYear($name='',$type='')
    {
        $rinfo=array('status'=>0,'info'=>'');
//        $redisModel=new RedisModel();
//        $tag=md5('searchYear'.$name.$type);
//        $redinfo=$redisModel->get($tag);
//        if($redinfo){
//            $rinfo['status']=1;
//            $rinfo['info']= $redinfo;
//            return;
//        }
        $arrinfo=array(
            'sign'=>md5($this->zh.$this->pwd.$this->safecode),
            'brand'=>$name,
            'series'=>$type,
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WebSearchYear',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
//            $redisModel->set($tag,$result['data'],$this->extime);
            $rinfo['info']= $result['data'];
        }
        return $rinfo;

    }


    //获取车型列表
    function searchModel($name='',$type='',$year='')
    {
        $rinfo=array('status'=>0,'info'=>'');
//        $redisModel=new RedisModel();
//        $tag=md5('searchModel'.$name.$type.$year);
//        $redinfo=$redisModel->get($tag);
//        if($redinfo){
//            $rinfo['status']=1;
//            $rinfo['info']= $redinfo;
//            return;
//        }
        $arrinfo=array(
            'sign'=>md5($this->zh.$this->pwd.$this->safecode),
            'brand'=>$name,
            'series'=>$type,
            'year'=>$year,
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WebSearchModel',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
//            $redisModel->set($tag,$result['data'],$this->extime);
            $rinfo['info']= $result['data'];
        }
        return $rinfo;

    }

    //获取车型列表
    function searchGood($name='',$type='')
    {
        $rinfo=array('status'=>0,'info'=>'');
        $arrinfo=array(
            'sign'=>md5($this->zh.$this->pwd.$this->safecode),
            'brand'=>$name,
            'series'=>$type,
        );
        $post_data=array(
            'result'=>json_encode($arrinfo),
        );
        $result=$this->curl($this->url.'WebSearchYear',$post_data);

        if($result['code']===0){
            $rinfo['status']=1;
            $rinfo['info']= $result['data'];
        }
        return $rinfo;

    }


    //用于字母归组
    private function getGroupData($data){
        $data = $this->groupByInitials($data, 'brand');
        $arrat=array();
        foreach ($data as $key=>$v){

            array_push($arrat,array('name'=>$key,'list'=>$data[$key]));

        }
        return $arrat;
    }

    /**
     * 二维数组根据首字母分组排序
     * @param array $data  二维数组
     * @param string $targetKey 首字母的键名
     * @return array    根据首字母关联的二维数组
     */
    public function groupByInitials(array $data, $targetKey = 'name')
    {
        $data = array_map(function ($item) use ($targetKey) {
            return array_merge($item, [
                'initials' => $this->getInitials($item[$targetKey]),
            ]);
        }, $data);
        $data = $this->sortInitials($data);
        return $data;
    }

    /**
     * 按字母排序
     * @param array $data
     * @return array
     */
    public function sortInitials(array $data)
    {
        $sortData = [];
        foreach ($data as $key => $value) {
            $sortData[$value['initials']][] = $value;
        }
        ksort($sortData);
        return $sortData;
    }

    /**
     * 获取首字母
     * @param string $str 汉字字符串
     * @return string 首字母
     */
    public function getInitials($str)
    {
        if (empty($str)) {return '';}
        $fchar = ord($str{0});
        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str{0});
        }

        $s1 = iconv('UTF-8', 'gb2312', $str);
        $s2 = iconv('gb2312', 'UTF-8', $s1);
        $s = $s2 == $str ? $s1 : $str;
        $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
        if ($asc >= -20319 && $asc <= -20284) {
            return 'A';
        }

        if ($asc >= -20283 && $asc <= -19776) {
            return 'B';
        }

        if ($asc >= -19775 && $asc <= -19219) {
            return 'C';
        }

        if ($asc >= -19218 && $asc <= -18711) {
            return 'D';
        }

        if ($asc >= -18710 && $asc <= -18527) {
            return 'E';
        }

        if ($asc >= -18526 && $asc <= -18240) {
            return 'F';
        }

        if ($asc >= -18239 && $asc <= -17923) {
            return 'G';
        }

        if ($asc >= -17922 && $asc <= -17418) {
            return 'H';
        }

        if ($asc >= -17417 && $asc <= -16475) {
            return 'J';
        }

        if ($asc >= -16474 && $asc <= -16213) {
            return 'K';
        }

        if ($asc >= -16212 && $asc <= -15641) {
            return 'L';
        }

        if ($asc >= -15640 && $asc <= -15166) {
            return 'M';
        }

        if ($asc >= -15165 && $asc <= -14923) {
            return 'N';
        }

        if ($asc >= -14922 && $asc <= -14915) {
            return 'O';
        }

        if ($asc >= -14914 && $asc <= -14631) {
            return 'P';
        }

        if ($asc >= -14630 && $asc <= -14150) {
            return 'Q';
        }

        if ($asc >= -14149 && $asc <= -14091) {
            return 'R';
        }

        if ($asc >= -14090 && $asc <= -13319) {
            return 'S';
        }

        if ($asc >= -13318 && $asc <= -12839) {
            return 'T';
        }

        if ($asc >= -12838 && $asc <= -12557) {
            return 'W';
        }

        if ($asc >= -12556 && $asc <= -11848) {
            return 'X';
        }

        if ($asc >= -11847 && $asc <= -11056) {
            return 'Y';
        }

        if ($asc >= -11055 && $asc <= -10247) {
            return 'Z';
        }

        return null;
    }








    private function curl($target,$post_data){
        $post_data=http_build_query($post_data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $target);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);          //单位 秒，也
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $return_str = curl_exec($curl);
        $result=json_decode($return_str,true);
        curl_close($curl);
        return $result;
    }

}