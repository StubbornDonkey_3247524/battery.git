<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace battery\portal\controller;


use think\Config;
use think\Request;

class BatteryBaseController extends BaseController
{

    function __construct(Request $request)
    {
        $wx_openid=$this->battery_openid_check();
        if($wx_openid){
            $this->battery_wx_autologin($wx_openid);
        }
        $filterInfo=$request->module().'|'.$request->controller().'|'.$request->action();
        $arrayAction=array(
            'portal|Brand|index',//模块|控制器|方法
            'portal|Brand|goods',//模块|控制器|方法
            'portal|Brand|details',//模块|控制器|方法
            'portal|Brand|searchbrand',//模块|控制器|方法
            'portal|Brand|searchseries',//模块|控制器|方法
            'portal|Brand|searchyear',//模块|控制器|方法
            'portal|Brand|searchmodel',//模块|控制器|方法
            'portal|Brand|getpay',//模块|控制器|方法
            'portal|Brand|payorder',//模块|控制器|方法
        );
        $isFilter=false;
        if(in_array($filterInfo,$arrayAction)){
            $isFilter=true;
        }
        if (battery_is_user_login()||$isFilter) {
            //已经登录时直接跳到首页
        } else {
            $this->redirect('login/index');
        }
        parent::__construct($request);
    }


    function battery_wx_autologin($openid)
    {
//        if ($openid) {
//            $hcwBaseModel=new HcwBaseModel();
//            $where['wx_md5']=md5($openid);
//            $info=$hcwBaseModel->name('user')->where($where)->find();
//            if ($info) {
//                battery_update_current_user($info);
//            }else{
//                battery_update_current_user(null);
//            }
//        }
    }

    function battery_openid_check()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {//检测是否微信浏览器
            $wx_appid=Config::get('WX_APPID');
            $wx_appsecret=Config::get('WX_APPSECRET');
            $wx_data_auth_key=Config::get('WX_DATA_AUTH_KEY');
            if (cookie('openid')) {//已有cookie，验证有效性
                $openidsign = md5(cookie('openid'), $wx_data_auth_key);
                if (!cookie('openidsign') or $openidsign != cookie('openidsign')) {//未通过openid验证，注销重新检测
                    cookie('openid', null);
                    cookie('openidsign', null);
                    battery_update_current_user(null);
                } else {
                    return cookie('openid');
                }
            } else {//跳转获取
                $request = Request::instance();
                $data=$request->param();


                if (!isset($data['code'])) {
                    $redirect_uri = url('index/index','',true,true);
                    $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $wx_appid . '&redirect_uri=' . $redirect_uri . '&response_type=code&scope=snsapi_userinfo&state=login#wechat_redirect';
                    $this->redirect($url);
                } else {
                    $code = $data['code'];
                    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' .$wx_appid . '&secret=' . $wx_appsecret . '&code=' . $code . '&grant_type=authorization_code';
                    // curl获取prepay_id
                    $result = curl_get_contents($url);
                    $result = json_decode($result, true);
                    $openid = $result['openid'];
                    cookie('openid', $openid, 604800);
                    $openidsign = md5($openid, $wx_data_auth_key);
                    cookie('openidsign', $openidsign, 604800);
                    return $openid;
                }
            }
        }
    }
    public function setTitleName($title='默认'){
        $this->assign('title_name',$title);
    }



}