<?php

use think\Db;
use cmf\lib\Storage;
use think\Config;


function getErrCode($key = 0)
{
    $info = array(
        0 => '请求失败',
        1 => '请求成功',
        10001 => '未登录'
    );
    return $info[$key];
}

/**
 * 判断前台用户是否登录
 * @return boolean
 */
function battery_is_user_login()
{
    $sessionUser = session('battery_user');
    return !empty($sessionUser);
}

/**
 * 更新当前登录前台用户的信息
 * @param array $user 前台用户的信息
 */
function battery_update_current_user($user){
    session('battery_user', $user);
}
/**
 * 获取当前登录的前台用户的信息，未登录时，返回false
 * @return array|boolean
 */
function battery_get_current_user()
{
    $sessionUser = session('battery_user');
    if (!empty($sessionUser)) {
        unset($sessionUser['battery_user_pass']); // 销毁敏感数据
        return $sessionUser;
    } else {
        return false;
    }
}

function curl_get_contents($url){
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);                //设置访问的url地址
    // curl_setopt($ch,CURLOPT_HEADER,1);               //是否显示头部信息
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);               //设置超时
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);   //用户访问代理 User-Agent
    curl_setopt($ch, CURLOPT_REFERER,$_SERVER['HTTP_HOST']);        //设置 referer
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);          //跟踪301
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        //返回结果
    $r=curl_exec($ch);
    curl_close($ch);
    return $r;
}

function hcw_get_md5($str='')
{
    return md5('xlC0TSdijC2ESHQD'.$str);
}

